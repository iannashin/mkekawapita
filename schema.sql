DROP TABLE IF EXISTS mkeka;

CREATE TABLE mkeka
(
    id                 INTEGER PRIMARY KEY AUTOINCREMENT,
    created            TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    playingOn          TIMESTAMP NOT NULL,
    league_id          INTEGER   NOT NULL,
    home_team_id       INTEGER   NOT NULL,
    away_team_id       INTEGER   NOT NULL,
    prediction_winner  INTEGER,
    result_winner      INTEGER,
    home_goals         INTEGER,
    away_goals         INTEGER,
    home_odds          INTEGER,
    away_odds          INTEGER,
    draw_odds          INTEGER,
    result_home_goals  INTEGER,
    result_away_goals  INTEGER,
    prediction_success BOOLEAN,
    source             TEXT,
    FOREIGN KEY (league_id) REFERENCES League (id),
    FOREIGN KEY (home_team_id) REFERENCES Team (id),
    FOREIGN KEY (away_team_id) REFERENCES Team (id)
);



DROP TABLE IF EXISTS League;

CREATE TABLE League
(
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    name    TEXT      NOT NULL
);


DROP TABLE IF EXISTS Team;

CREATE TABLE Team
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    created     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    name        TEXT      NOT NULL,
    description TEXT      NOT NULL
);