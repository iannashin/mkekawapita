from flask import Flask, render_template, request, jsonify, redirect, url_for
from service import create_team, create_league, create_mkeka, get_league, get_teams, \
    get_mkeka_by_league, delete_mkeka, del_league, del_team, get_all_dates, read_prediction_csv_file
from werkzeug.utils import secure_filename
import os

UPLOAD_FOLDER = '/data/db/'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'csv'}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/upload_file', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            Flask('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            Flask('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            read_prediction_csv_file(name=UPLOAD_FOLDER + '' + filename)
            return redirect(url_for('bets', name=UPLOAD_FOLDER + '' + filename))

    return render_template('upload.html')


@app.route('/team_list')
def team_list():
    teams = get_teams()
    return render_template('team_list.html', title='Mkekani', teams=teams)
    pass


@app.route('/edit_league')
def edit_team():
    return render_template('team.html', title='Pita Predections')


@app.route('/delete_team<id>')
def delete_team(id):
    del_team(id)
    return redirect("/team_list")


@app.route('/league_list')
def league_list():
    leagues = get_league()
    return render_template('league_list.html', title='Pita Predections', leagues=leagues)
    pass


@app.route('/edit_league')
def edit_league():
    return render_template('league.html', title='Pita Predections')


@app.route('/delete_league/<id>')
def delete_league(id):
    del_league(id)
    return redirect("/league_list")


@app.route('/bets')
def bets():
    mikeka = []
    leagues = get_league()

    for league in leagues:
        mkeka = get_mkeka_by_league(league_id=league['id'])
        mikeka.append(mkeka)

    return render_template('bets.html', title='Pita Predections',
                           mikeka=mikeka, leagues=leagues, dates=get_all_dates())
    pass


@app.route('/delete_bet/<id>')
def delete_bet(id):
    delete_mkeka(id)
    return redirect("/bets")


@app.route('/edit_bet/<id>')
def edit_bet(id):
    return render_template('mikeka.html', title='Pita Predections')


@app.route('/')
def index():
    mikeka = []
    leagues = get_league()
    for league in leagues:
        mkeka = get_mkeka_by_league(league_id=league['id'])
        mikeka.append(mkeka)

    return render_template('index.html', title='Mkekani',
                           mikeka=mikeka, leagues=leagues, dates=get_all_dates())
    pass


@app.route('/about')
def about():
    return render_template('about.html', title='Pita Predictions')


@app.route('/contact')
def contact():
    return render_template('contact.html', title='Pita Predictions')


@app.route('/filter', methods=['POST', 'GET'])
def result():
    if request.method == 'POST':
        mikeka = []
        leagues = get_league()
        if request.form.get('league') == 'all':
            for league in leagues:
                mkeka = get_mkeka_by_league(league_id=league['id'])
                mikeka.append(mkeka)
        else:
            mkeka = get_mkeka_by_league(league_id=request.form.get('league'))
            mikeka.append(mkeka)

        return render_template('index.html', title='Mkeka wa Pita',
                               mikeka=mikeka, leagues=leagues)
    return 'hey'


@app.route('/league', methods=['POST', 'GET'])
def league():
    if request.method == 'POST':
        name = request.form['name']

        msg = create_league(name)
        return redirect("/league_list")

    elif request.method == 'GET':
        return render_template('league.html')


@app.route('/team', methods=['POST', 'GET'])
def team():
    if request.method == 'POST':
        name = request.form['name']
        description = request.form['description']

        msg = create_team(name, description)
        return redirect("/team_list")

    elif request.method == 'GET':
        return render_template('team.html')


@app.route('/mikeka', methods=['POST', 'GET'])
def mikeka():
    if request.method == 'POST':
        home_team_id = request.form['home_team']
        away_team_id = request.form['away_team']
        league_id = request.form['league']
        prediction_winner = request.form['prediction']
        home_odds = request.form['home_odds']
        away_odds = request.form['away_odds']
        draw_odds = request.form['draw_odds']
        playingOn = request.form['playing_on']
        home_goals = request.form['home_goals']
        away_goals = request.form['away_goals']

        msg = create_mkeka(league_id, home_team_id, away_team_id, prediction_winner, home_goals, away_goals, home_odds,
                           away_odds,
                           draw_odds, playingOn)
        return redirect("/")


    elif request.method == 'GET':
        return render_template('mikeka.html', leagues=get_league(), teams=get_teams())


@app.route('/get_teams')
def get_team():
    rows = get_teams()
    OutputArray = []
    for row in rows:
        outputObj = {
            'id': row['id'],
            'name': row['name']}
        OutputArray.append(outputObj)
    return jsonify(OutputArray)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
