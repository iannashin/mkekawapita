from models import create_new_team, create_new_league, create_new_mkeka, get_all_league, \
    get_all_teams, mkeka_by_league, delete_mkeka_by_id, \
    delete_league_by_id, delete_team_by_id, get_dates, is_team_exits, is_league_exits, is_mkeka_exist
import csv


def create_league(name):
    return create_new_league(name)


def create_team(name, description):
    return create_new_team(name, description)


def create_mkeka(league_id, home_team_id, away_team_id, prediction_winner, home_goals, away_goals, home_odds, away_odds,
                 draw_odds, playingOn):
    return create_new_mkeka(league_id, home_team_id, away_team_id, prediction_winner, home_goals, away_goals, home_odds,
                            away_odds,
                            draw_odds, playingOn)


def get_league():
    return get_all_league()


def get_teams():
    return get_all_teams();


def get_mkeka_by_league(league_id):
    return mkeka_by_league(league_id)


def delete_mkeka(id):
    return delete_mkeka_by_id(id)


def del_league(id):
    return delete_league_by_id(id)


def del_team(id):
    return delete_team_by_id(id)


def get_all_dates():
    return get_dates()


def read_prediction_csv_file(name):

    file = open(name)
    csvreader = csv.reader(file)
    header = next(csvreader)
    print(header)
    rows = []
    for row in csvreader:
        rows.append(row)
        teams = row[0].split("-")
        home_team = is_team_exits(teams[0])
        away_team = is_team_exits(teams[1])
        prediction = row[1]
        goals = row[2].split(" ")
        home_odds = row[3]
        draw_odds = row[4]
        away_odds = row[5]
        league = row[6]
        league_saved = is_league_exits(league)
        playingOn = row[7]

    add_mkeka(rows)


def add_mkeka(rows):
    for row in rows:
        teams = row[0].split("-")
        home_team = is_team_exits(teams[0])
        away_team = is_team_exits(teams[1])
        prediction = row[1]
        goals = row[2].split(" ")
        home_odds = row[3]
        draw_odds = row[4]
        away_odds = row[5]
        league = row[6]
        league_saved = is_league_exits(league)
        playingOn = row[7]
        if not is_mkeka_exist(home_team[0]['id'], away_team[0]['id'], playingOn):
            create_new_mkeka(league_saved[0]['id'], home_team[0]['id'], away_team[0]['id'], prediction, goals[0],
                             goals[1], home_odds,
                             away_odds,
                             draw_odds, playingOn)
