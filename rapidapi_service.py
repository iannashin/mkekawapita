import json
from models import get_league_by_name

def get_all_leagues():
    f = open('static/prep_files/leagues.json',)
    data = json.load(f)

    for item in data['response']:
        # print(item['country']['name'] + ' ' + item['league']['name'])
        league = get_league_by_name(item['country']['name'] + ' ' +  item['league']['name'])
        if league:
            # print(item['league']['name'])
            print(item['country']['name'] + ' ' + item['league']['name'])

if __name__ == '__main__':
    get_all_leagues()