import random
import sys
from faker import Faker
from faker.providers import DynamicProvider
from app import db, Team, Mkeka, League

top_football_team_provider = DynamicProvider(
    provider_name="top_football_team",
    elements=["Man Utd", "Arsenal FC", "Man City", "Chelsea", "Liverpool", "Tottenham"],
)

mid_football_team_provider = DynamicProvider(
    provider_name="mid_football_team",
    elements=["Wolves", "Everton", "Crystal Palace", "Brentford", "Watford", "Leeds"],
)

league_provider = DynamicProvider(
    provider_name="league",
    elements=["English Premier League(EPL)", "La Liga", "Bundesliga", "Seria A", "Ligue 1"],
)

la_liga_top_football_team_provider = DynamicProvider(
    provider_name="la_liga_top_football_team",
    elements=["Real Madrid", "Barcelona", "Atheletico Madrid", "Valencia", "Sevilla", "Villareal"],
)

la_liga_mid_football_team_provider = DynamicProvider(
    provider_name="la_liga_mid_football_team",
    elements=["Getafe", "Granada", "Espanyol", "Osasuna", "Real Betis", "Mallorca"],
)


def create_fake_mkeka(n):
    """Generate fake Mkeka."""
    faker = Faker()
    faker.add_provider(top_football_team_provider)
    faker.add_provider(mid_football_team_provider)
    faker.add_provider(la_liga_mid_football_team_provider)
    faker.add_provider(la_liga_top_football_team_provider)
    faker.add_provider(league_provider)

    laliga = League(name="La Liga", active=True)
    db.session.add(laliga)

    epl = League(name="English Premier League(EPL)", active=True)
    db.session.add(epl)

    team_ars = Team(name="Manchester United", description="Manchester United", active=True)
    db.session.add(team_ars)

    team_ars = Team(name="Arsenal FC", description="Arsenal", active=True)
    db.session.add(team_ars)

    for i in range(n):
        epl_mkeka = Mkeka(league_id=1,
                                 home_team_id=1,
                                 away_team_id=2,
                                 prediction_winner=random.randint(0, 2),
                                 result_winner=random.randint(0, 2),
                                 home_goals=random.randint(0, 4),
                                 away_goals=random.randint(0, 4),
                                 home_odds=random.randint(20, 40),
                                 away_odds=random.randint(20, 40),
                                 draw_odds=random.randint(15, 20),
                                 result_home_goals=random.randint(0, 4),
                                 result_away_goals=random.randint(0, 4),
                                 prediction_success=random.randint(0, 1),
                                 source=faker.name())

        db.session.add(epl_mkeka)

        # la_liga_mkeka = Schema.Mkeka(league="La Liga",
        #                              home=faker.la_liga_top_football_team(),
        #                              away=faker.la_liga_mid_football_team(),
        #                              prediction_winner=random.randint(0, 2),
        #                              result_winner=random.randint(0, 2),
        #                              home_goals=random.randint(0, 4),
        #                              away_goals=random.randint(0, 4),
        #                              home_odds=random.randint(20, 40),
        #                              away_odds=random.randint(20, 40),
        #                              draw_odds=random.randint(15, 20),
        #                              result_home_goals=random.randint(0, 4),
        #                              result_away_goals=random.randint(0, 4),
        #                              prediction_success=random.randint(0, 1),
        #                              source=faker.name())
        #
        # db.session.add(la_liga_mkeka)
    db.session.commit()
    print(f'Added {n} fake mkeka to the database.')


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print('Pass the number of mkeka you want to create as an argument.')
        sys.exit(1)
    create_fake_mkeka(int(sys.argv[1]))
