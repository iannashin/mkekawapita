import sqlite3 as sql
from datetime import datetime

DATABASE =  '/data/db/ppredection.db'
# DATABASE =  'db/ppredection.db'


def create_new_league(name):
    try:
        with sql.connect(DATABASE) as con:
            cur = con.cursor()

            cur.execute("INSERT INTO League (name) VALUES(?)", [name])

            con.commit()
            msg = "Record successfully added"
    except Exception as e:
        con.rollback()
        msg = "error in insert operation"

    finally:
        con.close()
        return msg


def create_new_team(name, description):
    try:
        with sql.connect(DATABASE) as con:
            cur = con.cursor()

            cur.execute("INSERT INTO Team (name, description) VALUES(?,?)", [name, description])

            con.commit()
            msg = "Record successfully added"
    except Exception as e:
        con.rollback()
        msg = "error in insert operation"

    finally:
        con.close()
        return msg


def create_new_mkeka(league_id, home_team_id, away_team_id, prediction_winner, home_goals, away_goals, home_odds,
                     away_odds,
                     draw_odds, playingOn):
    try:
        with sql.connect(DATABASE) as con:
            cur = con.cursor()

            playingOn = datetime.strptime(playingOn, "%d-%m-%Y").strftime("%Y-%m-%d")

            cur.execute("INSERT INTO mkeka (league_id, home_team_id, away_team_id, prediction_winner, "
                        "home_goals, away_goals, home_odds, away_odds, draw_odds,"
                        " playingOn) VALUES(?,?,?,?,?,?,?, ?,?, ?)",
                        [league_id, home_team_id, away_team_id, prediction_winner, home_goals, away_goals, home_odds,
                         away_odds,
                         draw_odds, playingOn])

            con.commit()
            msg = "Record successfully added"
    except Exception as e:
        con.rollback()
        msg = "error in insert operation"
        print(e.with_traceback())

    finally:
        con.close()
        return msg


def get_all_league():
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row
    try:
        cur = con.cursor()
        cur.execute("select * from League order by id asc")
        return cur.fetchall();
    except Exception as e:
        print(e.with_traceback())
        con.close()


def get_league_by_name(name):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row
    try:
        cur = con.cursor()
        cur.execute("select * from league where name=?", [name])
        return cur.fetchall();
    except Exception as e:
        print(e.with_traceback())
        con.close()
        msg = "Delete failure"
    return msg


def get_all_teams():
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row
    try:
        cur = con.cursor()
        cur.execute("select * from Team order by name asc")
        return cur.fetchall();
    except Exception as e:
        print(e.with_traceback())
        con.close()


def mkeka_by_league(league_id):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row

    try:
        cur = con.cursor()
        cur.execute(
            "SELECT (select name from team where id = home_team_id) as home_team,(select name from team where id = away_team_id) as away_team, * FROM mkeka  where league_id=? and playingOn >= date('now')",
            [league_id])
        return cur.fetchall();
    except Exception as e:
        print(e.with_traceback())
        con.close()


def delete_mkeka_by_id(id):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row
    try:
        cur = con.cursor()
        cur.execute("delete from mkeka where id=?", [id])
        con.commit()
        msg = "Delete successfull"
    except Exception as e:
        print(e.with_traceback())
        con.close()
        msg = "Delete failure"
    return msg

def delete_league_by_id(id):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row
    try:
        cur = con.cursor()
        cur.execute("delete from League where id=?", [id])
        con.commit()
        msg = "Delete successfull"
    except Exception as e:
        print(e.with_traceback())
        con.close()
        msg = "Delete failure"
    return msg


def delete_team_by_id(id):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row
    try:
        cur = con.cursor()
        cur.execute("delete from team where id=?", [id])
        con.commit()
        msg = "Delete successfull"
    except Exception as e:
        print(e.with_traceback())
        con.close()
        msg = "Delete failure"
    return msg


def get_dates():
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row

    try:
        cur = con.cursor()
        cur.execute("select distinct playingOn from mkeka ")
        return cur.fetchall();

    except Exception as e:
        print(e.with_traceback())
        con.close()


def is_team_exits(name):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row

    try:
        cur = con.cursor()
        cur.execute("select * from Team where name=? ", [name])
        if len(cur.fetchall()) > 0:
            cur.execute("select * from Team where name=? ", [name])
            return cur.fetchall()
        else:
            return create_new_team(name, name)

    except Exception as e:
        print(e.with_traceback())
        con.close()


def is_league_exits(name):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row

    try:
        cur = con.cursor()
        cur.execute("select * from League where name=? ", [name])
        if len(cur.fetchall()) > 0:
            cur.execute("select * from League where name=? ", [name])
            return cur.fetchall()
        else:
            return create_new_league(name)

    except Exception as e:
        print(e.with_traceback())
        con.close()


def is_mkeka_exist(home, away, playingOn):
    con = sql.connect(DATABASE)
    con.row_factory = sql.Row

    try:
        cur = con.cursor()
        cur.execute("select * from mkeka where home_team_id=? and away_team_id=? and playingOn=? ", [home,away, playingOn])
        if len(cur.fetchall()) > 0:
            return True
        else:
            return False

    except Exception as e:
        print(e.with_traceback())
        con.close()
